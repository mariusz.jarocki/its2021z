app.controller('Home', [ '$http', function($http) {
    console.log('Kontroler Home wystartował')
    let ctrl = this

    ctrl.project = 'wybierz'
    ctrl.projects = []

    $http.get('/project').then(
        function(res) {
            ctrl.projects = res.data.records
        },
        function(err) {}
    )
}])